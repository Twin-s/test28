<?php
declare(strict_types=1);
namespace Database\Factories\Domain\Models;

use App\Domain\Models\CarMark;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Domain\Models\CarMark>
 */
class CarMarkFactory extends Factory
{
    public function modelName()
    {
        return CarMark::class;
    }

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [

            'mark_id' => '',
            'name' => 'Mercedes-Benz',
            'updated_at' => now(),
            'created_at' => now(),
        ];
    }
}
